import React, { Component } from "react";
import { dataGlasses } from "./DataGlasses";
import Detail from "./Detail";
import ImgGlass from "./ImgGlass";
import Model from "./Model";
import style from "./style.module.css";
export default class GlassesImg extends Component {
  state = {
    data: dataGlasses,
    detail: dataGlasses[1],
  };
  renderListGlass = () => {
    return this.state.data.map((item) => {
      return (
        <ImgGlass
          handleChangeDetailGlass={this.handleChangeDetailGlass}
          data={item}
          key={item.id.toString()}
        />
      );
    });
  };
  handleChangeDetailGlass = (data) => {
    this.setState({
      detail: data,
    });
  };
  render() {
    console.log(this.state.detail);
    return (
      <div className="container">
        <div className="row">
          <div className="col-6">
            <div className="row p-5 mt-5">{this.renderListGlass()}</div>
          </div>
          <div className="col-6">
            <Model />
          </div>
        </div>
        <Detail data={this.state.detail} />
      </div>
    );
  }
}
