import React, { Component } from "react";
import style from "./style.module.css";

export default class Detail extends Component {
  render() {
    let { url, name, desc } = this.props.data;
    return (
      <div className={`${style.move}`}>
        <img src={require(`${url}`)} width="35%" alt="" />
        <div className={`${style.moveModel}`}>
          <h3 className="text-left text-info">{name}</h3>
          <p className="text-left text-light">{desc}</p>
        </div>
      </div>
    );
  }
}
