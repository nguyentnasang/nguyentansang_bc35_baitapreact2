import React, { Component } from "react";

export default class ImgGlass extends Component {
  handelChangeClass = () => {
    return;
  };
  render() {
    let { url } = this.props.data;
    return (
      <div
        onClick={() => {
          this.props.handleChangeDetailGlass(this.props.data);
        }}
        className="col-4 mt-5 border mr-4"
      >
        <img src={require(`${url}`)} width="100px" alt="saa" />
      </div>
    );
  }
}
